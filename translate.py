from googletrans import Translator
import requests
import time

# def deepl_translate(text):
#     if len(text) > 140:
#         return google_translate(text)
#     url = 'https://multi-translate-public-api.rekon.uk/translate'
#     response = requests.get(url + '?source_text=' + text + '&to_language=en')
#     output = response.json()
#     if output is not None:
#         if 'translated_text' in output:
#             return output['translated_text']
#         else:
#             return output['error']
#     return 'Failed to Translate'

def deepl_translate(text):
    text = text[:140] if len(text) > 140 else text
    response = requests.post(
    "https://www2.deepl.com/jsonrpc",
    json = {
        "jsonrpc":"2.0",
        "method": "LMT_handle_jobs",
        "params": {
            "jobs":[{
                "kind":"default",
                "raw_en_sentence": text,
                "raw_en_context_before":[],
                "raw_en_context_after":[],
                "preferred_num_beams":4,
                "quality":"fast"
            }],
            "lang":{
                "user_preferred_langs":["EN"],
                "source_lang_user_selected":"JA",
                "target_lang":"EN"
            },
            "priority":-1,
            "commonJobParams":{},
            "timestamp": int(round(time.time() * 1000))
        },
        "id": 40890008
    })
    output = response.json()
    if output is not None:
        if 'result' in output:
            return output['result']['translations'][0]['beams'][0]["postprocessed_sentence"]
        if 'error' in output:
            return 'Error: ' + output['error']['message']
    return 'Failed to Translate'

def google_translate(text):
    translator = Translator()
    result = translator.translate(text)
    return result.text